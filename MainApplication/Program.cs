﻿using System;
using System.Collections.Generic;
using Events.Event;
using MainApplication.Events;
using MainApplication.Models;
using Newtonsoft.Json;

namespace MainApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var userModel = new UserModel();
            userModel.Username = "QQQ";
            userModel.Balance = 200;
            Console.WriteLine("Initial model state:");
            PrintState(userModel);
            ApplyInterestEvent interestEvent = new ApplyInterestEvent(userModel, (decimal) 1.1);
            PayDebtEvent debtEvent = new PayDebtEvent(userModel, 10);
            List<StateEvent> events = new List<StateEvent>();
            events.Add(interestEvent);
            events.Add(debtEvent);


            int input;
            while (true)
            {
                input = -1;
                PrintMenu();
                int.TryParse(Console.ReadLine(), out input);
                Console.Clear();
                // Console.WriteLine("1. List Events");
                // Console.WriteLine("2. Do Event");
                // Console.WriteLine("3. Undo Last Event");
                // Console.WriteLine("4. Redo Last Event");
                // Console.WriteLine("5. Print User");
                switch (input)
                {
                    case 1:
                        PrintEvents(events);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        PrintEvents(events);
                        Console.WriteLine("Please enter the event number you'd like to do.");
                        input = int.Parse(Console.ReadLine());
                        
                        if (input < 1 || input > events.Count)
                        {
                            Console.WriteLine("Please enter a valid event number!");
                        }
                        else
                        {
                            userModel.Do(events[input - 1]);
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 3:
                        if (userModel.Undoable)
                        {
                            userModel.UndoState();
                        }
                        else
                        {
                            Console.WriteLine("No events has been done.");
                        }

                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 4:
                        if (userModel.Redoable)
                        {
                            userModel.RedoState();
                        }
                        else
                        {
                            Console.WriteLine("No events has been undone.");
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 5:
                        PrintState(userModel);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Please enter a valid command!");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }

        static void PrintEvents(List<StateEvent> events)
        {
            Console.WriteLine("List of created events:");
            for (int i = 0; i < events.Count; i++)
            {
                Console.WriteLine("{0} -> {1}", i + 1, events[i].EventInformation);
            }
        }

        static void PrintMenu()
        {
            Console.WriteLine("1. List Events");
            Console.WriteLine("2. Do Event");
            Console.WriteLine("3. Undo Last Event");
            Console.WriteLine("4. Redo Last Event");
            Console.WriteLine("5. Print User");
        }

        static void PrintState(UserModel model)
        {
            Console.WriteLine(JsonConvert.SerializeObject(model, Formatting.Indented));
        }
    }
}
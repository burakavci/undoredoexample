﻿using Events.Event;

namespace MainApplication.Models
{
    public class UserModel : EventDrivenModel
    {
        public string Username { get; set; }
        public decimal Balance { get; set; }
    }
}
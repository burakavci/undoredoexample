﻿using System;
using System.Collections.Generic;

namespace Events.Event
{
    public abstract class EventDrivenModel
    {
        private Stack<StateEvent> DoneEvents;
        private Stack<StateEvent> UndoneEvents;

        public bool Undoable
        {
            get { return DoneEvents.Count != 0; }
        }
        
        public bool Redoable
        {
            get { return UndoneEvents.Count != 0; }
        }

        protected EventDrivenModel()
        {
            DoneEvents = new Stack<StateEvent>();
            UndoneEvents = new Stack<StateEvent>();
        }
        
        public void Do(StateEvent stateEvent)
        {
            stateEvent.Do();
            Console.WriteLine("{0} Event Done!", stateEvent.EventInformation);
            DoneEvents.Push(stateEvent);
        }

        public void UndoState()
        {
            var lastEvent = DoneEvents.Pop();
            lastEvent.Undo();
            Console.WriteLine("{0} Event Undone!", lastEvent.EventInformation);
            UndoneEvents.Push(lastEvent);
        }

        public void RedoState()
        {
            var nextEvent = UndoneEvents.Pop();
            nextEvent.Do();
            Console.WriteLine("{0} Event Redone!", nextEvent.EventInformation);
            DoneEvents.Push(nextEvent);
        }
    }
}
﻿using System;

namespace Events.Event
{
    public abstract class StateEvent
    {
        public abstract string EventInformation { get; }

        protected abstract Action DoAction { get; }
        protected abstract Action UndoAction { get; }

        protected internal void Undo()
        {
            UndoAction();
        }

        protected internal void Do()
        {
            DoAction();
        }
    }
}